import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './src/App.vue';
import Routes from './src/routes';

Vue.use(VueRouter);
const router = new VueRouter({
  routes: Routes,
  mode: 'history',
});
new Vue({
  render: (h) => h(App),
  router,
}).$mount('#app');
