const http = require('http');
const DateTime = require('./date');

http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' });
  const dateRes = { date: DateTime.DateTime() };
  res.end(JSON.stringify(dateRes));
}).listen(8084);
