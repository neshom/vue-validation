import Home from './components/Home.vue';
import Register from './components/Register.vue';
import List from './components/List.vue';

export default [
  { path: '/', component: Home },
  { path: '/register', component: Register },
  { path: '/list', component: List },
];
