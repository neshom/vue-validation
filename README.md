# vuejs Validation
its a very simple Form Validation(SFC) for developing vue js

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development
### requirements
you should have nodejs on your system

### Api 
pull repository
```
‍https://github.com/mhf-ir/test-openapi/
```

### Run Api 
```
nodejs backend.js
```

### Installing
```
npm i
```
## Production 
```
npm run build
```
## Deployment localhost:8080
```
npm run start
```

### Run Date Api 
```
nodejs backend.js
```

## Built With

* [webpack](https://webpack.js.org/) - webpack is a module bundler
* [vuejs](https://vuejs.org/) - javascript framework
* [eslint](https://eslint.org/) - pluggable and configurable linter tool for identifying and reporting on patterns in JavaScript(airbnb)

### styled with
* [sass](https://sass-lang.com/)-Sass is the most mature, stable, and powerful professional grade CSS extension language in the world.
* [bulma](https://bulma.io/)- Bulma is a free, open source CSS framework based on Flexbox. 

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
